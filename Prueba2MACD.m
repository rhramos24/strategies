clear
clc

%cambiando de directorio 
W=what;
handles.W=W.path;
cd C:\Users\SIGquantum4\AppData\Roaming\MetaQuotes\Terminal\AB5F107A56BD7D9D7DFC7F7921DA7F4B\MQL4\Files

%lectura de los datos
fid = fopen('_data of EURUSD period M1.csv');
out = textscan(fid,'%s%s%f%f%f%f%d','delimiter',',');
fclose(fid);

%regresando al directorio
cd(handles.W)

%Tomando 
high=out{3}(ceil(length(out{3})/1.4):length(out{3}));
low=out{4}(ceil(length(out{3})/1.4):length(out{3}));
close=out{5}(ceil(length(out{3})/1.4):length(out{3}));
open=out{6}(ceil(length(out{3})/1.4):length(out{3}));

%[macd,signal,macdh]  = indicators(cl              ,'macd'   ,short,long,signal)

%Por los periodos no funcionaba, pero ya est�
%vout=indicators([high,low,close,open],'macd',12,26,9);
vout=indicators([high,low,close,open],'macd',9,18,6);


%fpctk = vout (:, 1);
%fpctd = vout (:, 2);
%plot(1:length(fpctk),fpctk,'b',1:length(fpctd),fpctd,'g')
%title ('Estoc�stico r�pido para EURUSD')

macdevec = vout (:, 1);
lineaSenal = vout (:, 2);
histo=vout (:, 3);

%Inicializando las variables

accion=zeros(length(histo),1);
accion2=zeros(length(histo),1);
clave=zeros(length(histo),1);
detector=zeros(length(histo),1);

%Buscando los cambios de signo del macd, si cambia de + a - es venta, si
%cambia de - a + es compra.

clave(1)=NaN;
for i=2:length(histo)
    %if macdevec(i)*macdevec(i-1)< 0 && sign(macdevec(i))==-1
    if sign(macdevec(i)*macdevec(i-1))==-1 && sign(macdevec(i))==-1
        accion(i)=-1;
        clave(i)=close(i);
    %elseif macdevec(i)*macdevec(i-1)< 0 && sign(macdevec(i))==1
    elseif sign(macdevec(i)*macdevec(i-1))==-1 && sign(macdevec(i))==1
         accion(i)=1;
         clave(i)=close(i);
    else
        clave(i)=NaN;
    end
    
end

%Buscando los cambios de signo del histo, si cambia de + a - es venta, si
%cambia de - a + es compra.

clave(1)=NaN;
for i=2:length(histo)
    %if macdevec(i)*macdevec(i-1)< 0 && sign(macdevec(i))==-1
    
    if sign(histo(i)*histo(i-1))==-1 && sign(histo(i))==-1
        accion2(i)=-1;
        
        %clave(i)=close(i);
    %elseif macdevec(i)*macdevec(i-1)< 0 && sign(macdevec(i))==1
    elseif sign(histo(i)*histo(i-1))==-1 && sign(histo(i))==1
         accion2(i)=1;
         
         %clave(i)=close(i);
    %else
        %clave(i)=NaN;
    end
    
end


envio=[0; accion(length(histo))]


[accion, accion2]

%cambiando de directorio 
W=what;
handles.W=W.path;
cd C:\Users\SIGquantum4\AppData\Roaming\MetaQuotes\Terminal\AB5F107A56BD7D9D7DFC7F7921DA7F4B\MQL4\Files

csvwrite('magica.csv',envio)

%regresando al directorio
cd(handles.W)

figure

subplot(2,1,1)
candle(high,low,close,open,'k')

hold on
plot(clave,'gx','LineWidth',10)
title('EURUSD Diario')
hold off

subplot(2,1,2)
bar(macdevec)
plot(macdevec,'r')

hold on
plot(lineaSenal,'b')
bar(histo)

title('MACD')
hold off

