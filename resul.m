

%Funci�n que me indica que acci�n hacer, si comprar o vender al pasar por
%los indicadores t�cnicos.


function envio=resul(vin)

%Inicializando la variable de salida
envio=[];

% Input Data
       high = vin(:,1);
       low = vin(:,2);
       close = vin(:,3);
       open= vin(:,4);

%==========================================================================
%Resulado de MACD
 
vout=indicators([high,low,close,open],'macd',9,18,6);

macdevec = vout (:, 1);
lineaSenal = vout (:, 2);
histo=vout (:, 3);

macdevec = vout (:, 1);
lineaSenal = vout (:, 2);
histo=vout (:, 3);

%Inicializando las variables

accion=zeros(length(histo),1);
clave=zeros(length(histo),1);
detector=zeros(length(histo),1);

%Buscando los cambios de signo del macd, si cambia de + a - es venta, si
%cambia de - a + es compra.

for i=2:length(histo)
    if macdevec(i)*macdevec(i-1)< 0 && sign(macdevec(i))==-1
        accion(i)=-1;
        clave(i)=close(i);
    elseif macdevec(i)*macdevec(i-1)< 0 && sign(macdevec(i))==1
         accion(i)=1;
         clave(i)=close(i);
    else
        clave(i)=NaN;
    end
    
    
    if i+1==length(histo)
        clave(i+1)=NaN;
        break
    end
end

%envio=[close(length(histo)), accion(length(histo))];


%==========================================================================

%Resulado de CCI

vout=indicators([high,low,close],'cci',20,20,0.015);
%fpctk = vout (:, 1);
%fpctd = vout (:, 2);
%plot(1:length(fpctk),fpctk,'b',1:length(fpctd),fpctd,'g')
%title ('Estoc�stico r�pido para EURUSD')

cci = vout (:, 1);

%Tendencia de CCI si es menor o igual a menos 100 y es el m�nimo entonces
%da se�al de posible compra, si es mayor o igual a 100 y es un maximo da
%se�al de posible venta caso contrario escribe 0 que indica nada

tendCCI=zeros(length(cci),1);
for i=14:length(cci)
    if cci(i)<=-100 
        tendCCI(i)=min(cci(i));
        tendCCI(i)=-1;
    elseif cci(i)<=-100 & cci(i) > min(cci(i))
            tendCCI(i)=-1;
    else 
        tendCCI(i)=0;
            if cci(i)>=100 
            tendCCI(i)=max(cci(i));
            tendCCI(i)=1;
            elseif cci(i)>=100 & cci(i) <  max(cci(i))
                tendCCI(i)=1;
            else 
                tendCCI(i)=0;
            end
    end
end


%==========================================================================

%Resulado de Bollinger

[mid, uppr, lowr]=bollinger((close),12);

    %%%%%Hacemos respectiva comparaci�n entre el cierre y  la banda media.
  
  %%%%%Analizamos cuando los precios de cierrre se encuentran arriba o abajo de la
  %%%%%banda media, y nos da un indicador de venta
  
  Tendencia=close;
for i=20:length(close)
   if ((close(i)>=mid(i))&(close(i)-close(i-1))>=0)
           Tendencia(i)= 1;
   else
     if ((close(i)<mid(i))&(close(i)-close(i-1))<0)   
           Tendencia(i)= -1;
     else
         Tendencia(i)= 0;
     end
   end
end


%if   accion(length(close))~=0
    
%elseif tendCCI(length(cci))~=0
    
%elseif Tendencia(length(cci))~=0
        
envio=[close(length(histo)), accion(length(histo)), tendCCI(length(cci)),...
    Tendencia(length(cci))];
end
    
